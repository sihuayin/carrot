import Phaser from "phaser";

export const createButton = (obj: Phaser.Scene, normal: string, selected: string, cb: Function) => {
  const startBtn = obj.add.sprite(0, 0, normal).setInteractive();
        startBtn.on('pointerover', () => startBtn.setTexture(selected))
        startBtn.on('pointerdown', cb);
      startBtn.on('pointerout', () => { 
        startBtn.setTexture(normal)
      });
  return startBtn
}