class GameState {
  static readonly  MAX_GROUP_COUNT = 2
  static readonly  GROUP_MONSTER_COUNT = 4
  static current_level = 1
}

export default GameState