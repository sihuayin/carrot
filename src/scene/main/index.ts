import Phaser from "phaser";
import { createBGLayer } from './backgroundLayer'
import { createButton } from "../../button";

class MainScene extends Phaser.Scene
{
  isUnlock: string = '';

  curve: Phaser.Curves.CubicBezier | undefined;
  carrot: Phaser.GameObjects.Sprite | undefined;
  lockButton!: Phaser.GameObjects.Sprite;
  pop!: Phaser.GameObjects.Container;
  path: any
  constructor ()
  {
    super('MainScene');
  }

  preload () {
    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(440, 290, 320, 50);
            
    const width = this.cameras.main.width;
    const height = this.cameras.main.height;
    const loadingText = this.make.text({
        x: width / 2,
        y: height / 2 - 50,
        text: 'Loading...',
        style: {
            font: '20px monospace',
            color: '#ffffff'
        }
    });
    loadingText.setOrigin(0.5, 0.5);
            
    const percentText = this.make.text({
        x: width / 2,
        y: height / 2 - 5,
        text: '0%',
        style: {
            font: '18px monospace',
            color: '#ffffff'
        }
    });
    percentText.setOrigin(0.5, 0.5);
            
    const assetText = this.make.text({
        x: width / 2,
        y: height / 2 + 50,
        text: '',
        style: {
            font: '18px monospace',
            color: '#ffffff'
        }
    });
    assetText.setOrigin(0.5, 0.5);
            
    this.load.on('progress', function (value: any) {
        percentText.setText((value * 100) + '%');
        progressBar.clear();
        progressBar.fillStyle(0xffffff, 1);
        progressBar.fillRect(450, 300, 300 * value, 30);
    });
    
    this.load.on('fileprogress', function (file: any) {
        assetText.setText('Loading asset: ' + file.key);
    });
    this.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
        percentText.destroy();
        assetText.destroy();
    });
    // 背景图片
    this.load.image('background', 'assets/MainMenu/zh/front_bg.png');
    
    this.load.image('start_normal', 'assets/MainMenu/zh/front_btn_start_normal.png')
    this.load.image('start_press', 'assets/MainMenu/zh/front_btn_start_pressed.png')

    this.load.image('floor_normal', 'assets/MainMenu/zh/front_btn_floor_normal.png')
    this.load.image('floor_press', 'assets/MainMenu/zh/front_btn_floor_pressed.png')

    this.load.image('front_master', 'assets/MainMenu/front_monster_4.png')
    this.load.image('front_setting', 'assets/MainMenu/front_btn_setting_normal.png')

    this.load.image('front_help_hand', 'assets/MainMenu/front_monster_6_hand.png')
    this.load.image('front_help', 'assets/MainMenu/front_btn_help_normal.png')
    this.load.image('front_help_monster', 'assets/MainMenu/front_monster_6.png')

    this.load.image('front_left_yellow', 'assets/MainMenu/front_monster_3.png')
    this.load.image('front_left_green', 'assets/MainMenu/front_monster_1.png')
    
    this.load.image('front_smoke', 'assets/MainMenu/front_smoke_1.png')
    this.load.image('front_smoke_1', 'assets/MainMenu/front_smoke_3.png')
    this.load.image('front_smoke_2', 'assets/MainMenu/front_smoke_2.png')
    
    this.load.image('front_right_yellow', 'assets/MainMenu/front_monster_5.png')
    this.load.image('front_left_Blue', 'assets/MainMenu/front_monster_2.png')

    this.load.image('front_carrot', 'assets/MainMenu/front_carrot.png')
    this.load.image('front_bg', 'assets/MainMenu/front_front.png')

    this.load.image('front_btn_lock', 'assets/MainMenu/front_btn_floor_locked.png')
    this.load.image('front_pop_sure', 'assets/UI/btn_blue_m.png')
    this.load.image('front_pop_sure_pressed', 'assets/UI/btn_blue_m_pressed.png')

    this.load.image('front_pop_bg', 'assets/Common/bg/woodbg_notice.png')
    this.load.image('front_pop_info', 'assets/MainMenu/unlock_floor.png')
    this.load.image('front_pop_sure_ok', 'assets/UI/zh/btn_blue_m_ok.png')
    this.load.image('front_pop_cancel', 'assets/UI/btn_green_m.png')
    this.load.image('front_pop_cancel_pressed', 'assets/UI/btn_green_m_pressed.png')
    this.load.image('front_pop_cancel_text', 'assets/UI/zh/btn_green_m_cancel.png')
  
    this.load.audio('front_bg_music', 'assets/Sound/MainMenu/BGMusic.mp3')
    this.load.audio('front_select_music', 'assets/Sound/MainMenu/Select.mp3')
  }

  create () {
    // 创建背景层
    createBGLayer(this)
    // 播放背景音乐
    // this.sound.add('front_bg_music', { loop: true }).play()
    // 添加按钮菜单
    this.createMenu()

    this.createSet()
    this.createHelp()
    this.createBackMonster()
    this.createBackSmoke()
    this.createForeMonster()
    this.createForeSmoke()
    this.createCarrot()
    this.createForeground()
    this.renderLayer()
  }

  update() {
    const position = this.curve?.getPoint(this.path.t, this.path.vec);
    this.carrot?.setPosition(position.x, position.y)
  }

  createMenu() {
    let selectMusic = this.sound.add('front_select_music')
    // 开始按钮
    const startBtn = createButton(this, 'start_normal', 'start_press', () => {
      selectMusic.play()
      this.scene.stop();
      this.scene.start('ChooseLevel');
    })
    startBtn.setPosition(this.cameras.main.centerX - 8, this.cameras.main.centerY - 75)

    // 天天向上
    const floorBtn = createButton(this, 'floor_normal', 'floor_press', () => {
      selectMusic.play()
      if (this.isUnlock === 'NO') {
        console.log('jiesuo') 
        this.pop.setVisible(true)
      }
    })

    floorBtn.setPosition(this.cameras.main.centerX - 8, this.cameras.main.centerY + 45)

    // locked front_btn_lock
    this.isUnlock = window.localStorage.getItem('isUnLock') || 'NO';
    this.lockButton = this.add.sprite(floorBtn.x + 135, floorBtn.y / 2 + 160, 'front_btn_lock')
    if (this.isUnlock !== 'NO') {
      this.lockButton.destroy()
    }
  }

  createSet() {
    // 添加怪物
    let master = this.add.sprite(0, 0, 'front_master')
    // 添加设置
    let setting = this.add.sprite(32, 45, 'front_setting')
    var container = this.add.container(this.cameras.main.centerX - 350, this.cameras.main.height - 490, [ master, setting ])
    this.tweens.add({
      targets: container,
      props: {
        y: { value: this.cameras.main.height - 480, duration: 1000, ease: 'Power0', yoyo: true,repeat: -1 }
      }
    });
  }
  
  createHelp() {
    // 添加帮助图片
    let helpHandBt = this.add.sprite(0, 0, 'front_help_hand')
    let helpBt = this.add.sprite(5, -150, 'front_help')
    let helpContainer1 = this.add.container(0, 0, [helpHandBt, helpBt])
    // 手臂动画
    this.tweens.add({
      targets: helpContainer1,
      props: {
        angle: { value: 5, duration: 800, ease: 'Power0', yoyo: true,repeat: -1 }
      }
    });

    // 身体
    let helpMonster = this.add.sprite(125, 0, 'front_help_monster')
    this.add.container(this.cameras.main.centerX + 270, this.cameras.main.height - 270, [helpContainer1, helpMonster])
  }

  createBackMonster() {
    // 背景的左侧怪物 
    let leftYellow = this.add.sprite(this.cameras.main.centerX - 360, this.cameras.main.height - 220, 'front_left_yellow')
    this.tweens.add({
      targets: leftYellow,
      props: {
        y: { value: this.cameras.main.height - 225, duration: 800, ease: 'Power0', yoyo: true,repeat: -1 }
      }
    });

    // 背景的左侧绿色怪物 
    let leftGreen = this.add.sprite(this.cameras.main.centerX - 300, this.cameras.main.height - 185, 'front_left_green')
    this.tweens.add({
      targets: leftGreen,
      props: {
        x: { value: this.cameras.main.centerX - 303, duration: 700, ease: 'Power0', yoyo: true,repeat: -1 }
      }
    });
  }

  createBackSmoke() {
    // 背景烟
    this.add.sprite(this.cameras.main.centerX - 410, this.cameras.main.height - 188, 'front_smoke')
    this.add.sprite(this.cameras.main.centerX + 450,  this.cameras.main.height - 190, 'front_smoke_1')
  }

  createForeMonster() {
    let rightYellow = this.add.sprite(this.cameras.main.centerX + 290,  this.cameras.main.height - 185, 'front_right_yellow')
    this.tweens.add({
      targets: rightYellow,
      props: {
        x: { value: this.cameras.main.centerX + 295, duration: 850, ease: 'Power0', yoyo: true,repeat: -1 }
      }
    });

    let leftBlue = this.add.sprite(this.cameras.main.centerX - 300,  this.cameras.main.height - 150, 'front_left_Blue')
    this.tweens.add({
      targets: leftBlue,
      x: this.cameras.main.centerX - 220,
      y: this.cameras.main.height - 170,
      ease: 'Power1',
      duration: 200,
      onComplete: () => {
        this.tweens.add({
          targets: leftBlue,
          props: {
            y: { value: this.cameras.main.height - 175, duration: 850, ease: 'Power0', yoyo: true,repeat: -1 }
          }
        })
      }
    });
  }

  createForeSmoke() {
    this.add.sprite(this.cameras.main.centerX + 320, this.cameras.main.height - 150, 'front_smoke_2')
  }

  createCarrot() {
    const carrot = this.add.sprite(this.cameras.main.centerX + 320, this.cameras.main.height - 120, 'front_carrot')
    // 放大效果
    carrot.scale = 0.7;
    this.carrot = carrot;

    this.tweens.add({
      targets: carrot,
      props: {
        scaleX: { value: 1, duration: 800, ease: 'Power0'},
        scaleY: { value: 1, duration: 800, ease: 'Power0'}
      }
    });
    
    this.path = { t: 0, vec: new Phaser.Math.Vector2()};

    var startPoint = new Phaser.Math.Vector2(this.cameras.main.centerX + 320, this.cameras.main.height - 120);
    var controlPoint1 = new Phaser.Math.Vector2(this.cameras.main.centerX + 400, this.cameras.main.height - 100);
    var controlPoint2 = new Phaser.Math.Vector2(this.cameras.main.centerX + 120, this.cameras.main.height);
    var endPoint = new Phaser.Math.Vector2(this.cameras.main.centerX + 100, this.cameras.main.height - 20);

    this.curve = new Phaser.Curves.CubicBezier(startPoint, controlPoint1, controlPoint2, endPoint);

    this.tweens.add({
        targets: this.path,
        t: 1,
        ease: 'Power1',
        duration: 800
    });
  }

  createForeground() {
    this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'front_bg')
  }

  renderLayer () {
    var bg1 = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'front_pop_bg')
    var bg2 = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY - 100, 'front_pop_info')

    var bg3 = this.add.container(400, 420)
    // 开始按钮
    var sureButton = createButton(this, 'front_pop_sure', 'front_pop_sure_pressed', () => {
      // 解锁
      // 发送请求处理
      // 本地保存
      window.localStorage.setItem('isUnLock', 'YES')
      // 清除锁
      this.lockButton && this.lockButton.destroy()
      this.isUnlock = 'YES'
      this.pop.setVisible(false)
    })
    
    var sureText = this.add.sprite(0,0 ,'front_pop_sure_ok')
    bg3.add(sureButton)
    bg3.add(sureText)
 

    var bg4 = this.add.container(760, 420)
    // 取消按钮
    var cancelButton = createButton(this, 'front_pop_cancel', 'front_pop_cancel_pressed', () => {
      this.pop.setVisible(false)
    })
    
    var cancelText = this.add.sprite(0,0 ,'front_pop_cancel_text')

    bg4.add(cancelButton)
    bg4.add(cancelText)

    this.pop = this.add.container(0,0, [bg1, bg2, bg3, bg4])
    this.pop.setVisible(false)
  }
}

export default MainScene