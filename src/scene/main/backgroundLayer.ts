import Phaser from "phaser"

export const createBGLayer = (scene: Phaser.Scene) => {
  const bg = scene.add.sprite(0, 0, 'background');
  bg.setPosition(scene.cameras.main.centerX, scene.cameras.main.centerY)
  const layer = scene.add.layer();

  layer.add(bg);
}