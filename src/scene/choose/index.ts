import Phaser from "phaser";
import GameState from "../../state";
import { LevelEffect } from './effect'

let controls: Phaser.Cameras.Controls.SmoothedKeyControl

export class ChooseLevel extends Phaser.Scene {
  bgContainer!: Phaser.GameObjects.Container;
  buttonContainer!: Phaser.GameObjects.Container;
  leftContainer!: Phaser.GameObjects.Container;
  midContainer!: Phaser.GameObjects.Container;

  constructor() {
    super('ChooseLevel');
  }
  preload() {
    // this.load.audio('front_bg_music', 'assets/Sound/MainMenu/BGMusic.mp3')
    this.load.tilemapTiledJSON('map', 'assets/ChooseLevel/Map/TiledMap.json');
    // res/ChooseLevel/Map/stage_map_" + i + ".png
    for (var i = 0; i < 14; i++) {
      this.load.image(`stage_map_${i}`, "assets/ChooseLevel/Map/stage_map_" + i + ".png");
    }

    for (var i = 1; i < 135; i++) {
      this.load.image(`route_${i}`, "assets/ChooseLevel/Route/route_" + i + ".png");
    }

    this.load.image('choose_level_adv','assets/ChooseLevel/stagepoint_adv.png')
    this.load.image('choose_level_boss', 'assets/ChooseLevel/stagepoint_gate.png')
    this.load.image('choose_level_time', 'assets/ChooseLevel/stagepoint_time.png')
    this.load.image('choose_level_chance', 'assets/ChooseLevel/stagepoint_chance.png')
    this.load.image('choose_level_leftbg', 'assets/ChooseLevel/stagemap_toolbar_leftbg.png')
    this.load.image('choose_level_home_button', 'assets/ChooseLevel/stagemap_toolbar_home.png')
    this.load.image('choose_level_shop_button', 'assets/ChooseLevel/stagemap_toolbar_shop.png')
    this.load.image('choose_level_ranking_button', 'assets/ChooseLevel/stagemap_toolbar_leaderboard.png')
    this.load.image('choose_level_discount', 'assets/ChooseLevel/zh/discount_tag_stone.png')
    this.load.image('numbers', 'assets/ChooseLevel/discount.png');
    this.load.image('choose_level_rightbg', 'assets/ChooseLevel/stagemap_toolbar_rightbg.png')
    this.load.image('choose_level_overten', 'assets/ChooseLevel/zh/stagemap_toolbar_overten.png')
    this.load.image('level_effect', 'assets/ChooseLevel/stagemap_local.png')
  }
  create() {
    const map = this.add.tilemap('map')
    // this.sound.add('front_bg_music', { loop: true }).play()
    this.createBG()

    var cursors = this.input.keyboard?.createCursorKeys();

    var controlConfig = {
      camera: this.cameras.main,
      left: cursors?.left,
      right: cursors?.right,
      up: cursors?.up,
      down: cursors?.down,
      acceleration: 0.04,
      drag: 0.0005,
      maxSpeed: 1.0
    };

    controls = new Phaser.Cameras.Controls.SmoothedKeyControl(controlConfig);

    this.buttonContainer = this.add.container(0, 0)
    this.buttonContainer.setDepth(10)
  
    const layer = map.getObjectLayer('point');

    let objs: any  = layer?.objects || [];
    for (var i = 0; i < objs.length; i++) {
        var button = this.add.sprite(0,0, 'choose_level_adv').setInteractive();
        // this.scorllView.addChild(button, this.zOrderMap.levelButton, i);
        // this.routeButtonArray.push(button);

        // 编辑器中配置的属性
        if (objs[i].isBoos == "YES") {
          button.setTexture('choose_level_boss')
        }else if (objs[i].isTime == "YES") {
          button.setTexture('choose_level_time')
        }else if (objs[i].isChange == "YES"){
          button.setTexture('choose_level_chance')
        }

        button.setPosition(objs[i].x, objs[i].y);
        this.buttonContainer.add(button);
        ((i) => {
          button.on('pointerdown', () => { 
            // 停止音乐
            // 关卡设置
            // 进入不同等级的游戏
            GameState.current_level = i + 1
            this.scene.stop();
            this.scene.start('GamePlay');
          }, this)
        })(i)
        
        // button.setTag(i);
        // button.addTouchEventListener(this.onLevelButtonEvent, this);
    }
    this.loadLevel(2)
    // this.loadLevelEffects(2);
    this.loadUI()
  }

  loadLevel(level: number) {
    this.loadRoute(level)
    this.loadLevelEffects(level)
  }

  loadRoute (level: number) {
    for (let i = 0; i < level - 1; i ++) {
      var route = this.add.sprite(0, 0, 'route_' + (i + 1))
      if (i %10 == 9) {
        route.setOrigin(0, 0.5)
      }

      route.x = route.width / 2 + Math.floor(i / 10) * route.width;
      route.y = this.cameras.main.height / 2
    }
  }

  loadLevelEffects (level: number) {
    var button = this.buttonContainer.getAt(level-1)
    
    for (var i = 0; i < 3; i++) {
      this.time.delayedCall(250 * i, () => {
        console.log(button)
        var effect = new LevelEffect(this, (button as any).x, (button as any).y)
        
        this.add.existing(effect)
      }, [], this);
    }
    
  }

  update (time:any, delta: any) {
    controls?.update(delta);
  }

  createBG() {
    this.bgContainer = this.add.container(0, 0)
    
    const tiles = []
    let nextPosX = 0

    for (var i = 0; i < 14; i++) {
      // var tile = map.addTilesetImage(`stage_map_${i}`);
      // tiles.push(tile)
      let imageView = this.add.sprite(0, 0, `stage_map_${i}`).setInteractive({ draggable: true })
      imageView.setOrigin(0, 0.5)
      imageView.setPosition(nextPosX, this.cameras.main.height / 2);
      this.bgContainer.add(imageView);
      
      
      nextPosX += imageView.width;
    }

    this.cameras.main.setBounds(0, 0, nextPosX, this.cameras.main.height);
  }

  loadUI () {
    this.leftContainer = this.add.container(0, 0)
    let leftPanle = this.add.image(0, 0, 'choose_level_leftbg')
    leftPanle.setOrigin(0, 0)
    this.leftContainer.add(leftPanle)

    this.addHomeButton()
    this.addShopButton()
    this.addRankingButton()
    this.loadDiscountButton();
    this.loadLifeStar()
  }

  addHomeButton() {
    let homeButton = this.add.sprite(0, 0, 'choose_level_home_button').setInteractive()
    // homeButton.setScale(0.2, 0.2)
    homeButton.setOrigin(0, 0)
    homeButton.setPosition(10, 0)
    homeButton.on('pointerdown', () => {
      // this.sence.run()
    });

    this.leftContainer.add(homeButton)
  }

  addShopButton () {
    let shopButton = this.add.sprite(0, 0, 'choose_level_shop_button').setInteractive()
    shopButton.setOrigin(0, 0)
    shopButton.setPosition(111, 0)
    shopButton.on('pointerdown', () => {
      // this.sence.run()
    });

    this.leftContainer.add(shopButton)
  }

  addRankingButton () {
    let rankingButton = this.add.sprite(0, 0, 'choose_level_ranking_button').setInteractive()
    rankingButton.setOrigin(0, 0)
    rankingButton.setPosition(212, 0)
    rankingButton.on('pointerdown', () => {
      // this.sence.run()
    });

    this.leftContainer.add(rankingButton)
  }

  loadDiscountButton () {
    this.midContainer = this.add.container(0, 0)
    var discount = this.add.sprite(this.cameras.main.centerX, 0, 'choose_level_discount').setInteractive()
    discount.setOrigin(0.5, 0)
    discount.on('pointerdown', () => {
      console.log('cu xiao')
    })

    this.midContainer.add(discount)
    var config = {
      image: 'numbers',
      width: 24,
      height: 30,
      chars: Phaser.GameObjects.RetroFont.TEXT_SET8,
      charsPerRow: 8,
      spacing: { x: 0, y: 0 }
    };

    this.cache.bitmapFont.add('numbers', Phaser.GameObjects.RetroFont.Parse(this, config));
    var dynamic = this.add.bitmapText(this.cameras.main.centerX + 35, 15, 'numbers', '7');
    dynamic.setOrigin(0, 0)
    this.midContainer.add(dynamic)
  }

  loadLifeStar() {
    let bt = this.add.sprite(0, 0, 'choose_level_rightbg')
    bt.setOrigin(1, 0)
    bt.setPosition(this.cameras.main.width , 0)
    bt.on('pointerdown', () => {
      console.log('leell')
    })
    let starImage = this.add.image(0, 0, 'choose_level_overten')
    starImage.setOrigin(1, 0)
    starImage.setPosition(this.cameras.main.width, 0);

    let text = this.add.text(this.cameras.main.width - 60, 0, '010').setFontFamily('Arial').setFontSize(24)
    text.setOrigin(1, 0)
  }
}