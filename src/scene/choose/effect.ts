import Phaser from "phaser";

export class LevelEffect extends Phaser.GameObjects.Sprite {

  constructor (scene: Phaser.Scene, x: number, y: number, texture?: string | Phaser.Textures.Texture, frame?: string | number)
  {
      super(scene, x, y, 'level_effect', frame);

      this.setPosition(x, y);

      this.cusPlay(scene)
  }

  cusPlay(scene: Phaser.Scene) {
    this.setScale(0.35, 0.35)

    this.setAlpha(1)

    scene.time.addEvent({ delay: 640, callback: () => {
      scene.tweens.add({
        targets: this,
        alpha: {value: 0, duration: 560, ease: 'Expo.easeOut'}
      })
    }, callbackScope: this, loop: false })

    // 
    this.ani(scene)
  }
  ani (scene: Phaser.Scene) {
    scene.tweens.add({
      targets: this,
      scaleX: { value: 1.35, duration: 800, ease: 'Power1' },
      scaleY: { value: 1.35, duration: 800, ease: 'Power1' },
      yoyo: false,
      onComplete: () => {
        scene.time.addEvent({ delay: 1000, callback: () => {
          this.cusPlay(scene)
        }, callbackScope: this, loop: false })
      }
    })
  }
}