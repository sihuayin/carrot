import Phaser from "phaser";
import { createButton } from "../../button";

export class GameResult extends Phaser.Scene {


  constructor() {
    super('GameResult');
  }
  preload() {
    this.load.image('game_win_bg', "assets/GameResult/Win/win_bg.png")
    this.load.image('game_lose_bg', "assets/GameResult/Lose/lose_bg.png")
    this.load.image('win_lose_bg', 'assets/GameResult/GameOver/winlose_bg.png')
    this.load.image('title_win_whb', 'assets/GameResult/Win/win_title_whb.png')
    this.load.image('title_lose_whb', 'assets/GameResult/Lose/lose_title_whb.png')
    this.load.image('cup_gold', 'assets/GameResult/Win/cup_gold.png')
    this.load.image('lose_rip', 'assets/GameResult/Lose/lose_rip.png')
    this.load.image('win_getstone', 'assets/GameResult/Win/win_getstone.png')
    this.load.image('lose_adv', 'assets/GameResult/Lose/lose_adv.png')
    this.load.image('winlose_winover', 'assets/GameResult/Win/winlose_winover.png')
    this.load.image('winlose_loseover', 'assets/GameResult/Lose/winlose_loseover.png')

    this.load.image('btn_blue_s', 'assets/UI/btn_blue_s.png')
    this.load.image('btn_blue_s_pressed', 'assets/UI/btn_blue_s_pressed.png')
    this.load.image('winlose_home', 'assets/GameResult/GameOver/winlose_home.png')

    this.load.image('btn_green_b', 'assets/UI/btn_green_b.png')
    this.load.image('btn_green_b_pressed', 'assets/UI/btn_green_b_pressed.png')
    this.load.image('win_continue', 'assets/GameResult/Win/win_continue.png')
    this.load.image('lose_retry', 'assets/GameResult/Lose/lose_retry.png')
    
    this.load.image('win_weibo', 'assets/GameResult/GameOver/win_weibo.png')
    
  }
  create() {
    this.createBG()
    this.createMain()
  }

  createBG() {
    const bg = false ? 'game_win_bg' : 'game_lose_bg'
    let node = this.add.sprite(0, 0, bg)
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY)

    node = this.add.sprite(0, 0, 'win_lose_bg')
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY)
  }

  createMain(){
    this.loadTitle();
    this.loadTitleIcon()
    this.loadAdvance()
    this.loadTipPanel();
    this.loadMenu()
  }

  loadTitle() {
    const bg = true ? 'title_win_whb' : 'title_lose_whb'
    let node = this.add.sprite(0, 0, bg)
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY - node.height/ 2)
  }

  loadTitleIcon() {
    const bg = true ? 'cup_gold' : 'lose_rip'
    let node = this.add.sprite(0, 0, bg)
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY - node.height/ 2)
  }

  loadAdvance() {
    const isWin = true
    const bg = isWin ? 'win_getstone' : 'lose_adv'
    let node = this.add.sprite(0, 0, bg)
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY - node.height/ 2)

    if (isWin) {
      const stoneText = this.add.text(0, 0, '04').setFontSize(38).setShadow(1, 1);
      stoneText.setPosition(node.width - 140, node.height / 2);
    } else {
      this.add.bitmapText(0, 0, 'atari', "10")
        .setFontSize(38);
      this.add.bitmapText(0, 0, 'atari', "100")
        .setFontSize(38);
    }
  }

  loadTipPanel() {
    const isWin = true
    const bg = isWin ? 'winlose_winover' : 'winlose_loseover'
    let node = this.add.sprite(0, 0, bg)
    node.setPosition(this.cameras.main.centerX, this.cameras.main.centerY - node.height/ 2)

    if (isWin) {} else {}
  }

  loadMenu() {
    const isWin = true
    var posY = 90;
    var offsetX = 260;
    var bg4 = this.add.container(this.cameras.main.centerX - offsetX, this.cameras.main.height - 90)

    const homeBtn = createButton(this, 'btn_green_b', 'btn_green_b_pressed', () => {
      // selectMusic.play()
      // this.scene.stop();
      // this.scene.start('ChooseLevel');
    })

    var homeIcon = this.add.sprite(0, 0, 'winlose_home')
    // homeIcon.setPosition(homeBtn.width / 2, homeBtn.height / 2);
    bg4.add(homeBtn)
    bg4.add(homeIcon)
    

    var bg1 = this.add.container(this.cameras.main.centerX - offsetX, this.cameras.main.centerY)

    const playBtn = createButton(this, 'btn_blue_s', 'btn_blue_s_pressed', () => {
      // selectMusic.play()
      // 根据输赢，跳去不同的地方
      this.scene.stop();
      this.scene.start('ChooseLevel');
    })
    // homeBtn.setPosition()

    var winIcon = this.add.sprite(0, 0, isWin? 'win_continue' : 'lose_retry')
    // homeIcon.setPosition(homeBtn.width / 2, homeBtn.height / 2);
    bg1.add(playBtn)
    bg1.add(winIcon)

    var bg2 = this.add.container(this.cameras.main.centerX + offsetX, this.cameras.main.height - posY)

    const weiboBtn = createButton(this, 'btn_green_b', 'btn_green_b_pressed', () => {
      // selectMusic.play()
      // this.scene.stop();
      // this.scene.start('ChooseLevel');
    })

    var weiboIcon = this.add.sprite(0, 0, 'win_weibo')
    // homeIcon.setPosition(homeBtn.width / 2, homeBtn.height / 2);
    bg2.add(weiboBtn)
    bg2.add(weiboIcon)
  }
  update (time:any, delta: any) {

  }
}