import Phaser from 'phaser'
import MainScene from './scene/main';
import { ChooseLevel } from './scene/choose';
import { GamePlay } from './scene/play'
import { GameResult } from './scene/result';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  width: 1136,
  height: 640,
  backgroundColor: '#3498db',
  physics: {
      default: 'arcade',
      arcade: {
          gravity: { y: 200 }
      }
  },
  scene: [GamePlay, MainScene, ChooseLevel, GameResult]
};

const game = new Phaser.Game(config);