import Phaser from "phaser"

export class Bullet extends  Phaser.Physics.Arcade.Sprite {
  speed: number
  
  constructor (scene: Phaser.Scene) {
    super(scene, 0, 0, 'PBottle31')

    this.speed = 10
  }

  fire  (x: number, y: number) {
    this.setPosition(x, y);

    this.setActive(true);
    this.setVisible(true);
  }

}