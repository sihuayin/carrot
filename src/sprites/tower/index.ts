import Phaser from "phaser"

export class TowerPanel extends Phaser.GameObjects.Sprite {
  callback = () => {}
  cel: any
  row: any;
  bot!: Phaser.GameObjects.Sprite

  constructor (scene: Phaser.Scene, x: number, y: number, texture: string | Phaser.Textures.Texture = 'select_01', frame?: string | number) {
    super(scene, x, y, texture, frame)
    this.scene = scene
  }

  loadProperty (args: any) {
    if (args.cel < 0) {
      console.log('TowerPanel: 列数必须大于0')
      return ;
    }
    if (args.row < 0) {
      console.log('TowerPanel: 行号必须大于0')
      return ;
    }
    if (args.x === undefined) {
      console.log('TowerPanel: x坐标必须制定');
      return ;
    }

    if (args.y === undefined) {
      console.log('TowerPanel: Y坐标必须制定');
      return ;
    }

    this.cel = args.cel;
    this.row = args.row;
    // this.x = args.x + this.width / 2;
    // this.y = args.y + this.height / 2;
    console.log(args.x + this.width / 2, args.y + this.height / 2)
    this.setPosition(args.x + this.width / 2, args.y + this.height / 2)
    this.loadTower()
    // this.scene.input.on('gameobjectdown', (pointer, gameObject) => {
    //   pointer.event.preventDefault()
    //   pointer.event.stopPropagation()
    //   // gameObject.destroy();
    //   if (gameObject.getData('name') === 'bottle') {
    //     this.callback()
    //   }
    //   return false;
    // });
  }

  loadTower () {
    this.bot = this.scene.add.sprite(0, 0, 'Bottle01').setInteractive()
    this.bot.setName('bottle')
    this.bot.setDepth(10)
    this.bot.setOrigin(0.5, 0)
    if (this.y > this.scene.cameras.main.height - 2 * this.height) {
      this.bot.setPosition(this.x , this.y - this.height - this.bot.height / 2)
    } else {
      this.bot.setPosition(this.x, this.y + this.height/ 2)
    }

    this.bot.on('pointerdown', (pointer: any) => {
      pointer.event.preventDefault()
      this.scene.input.stopPropagation()

        this.callback()
      
    })
  }

  setCallback(callback: () => void) {
    this.callback = callback
  }

  getData() {
    return {
      cel: this.cel,
      row: this.row,
      x: this.x,
      y: this.y,
      bot: {
        name: this.bot.name
      }
    }
  }

  destroyAll () {
    this.scene.input.off('gameobjectdown')
    this.destroy()
    this.bot && this.bot.destroy()
    return false
  }
}