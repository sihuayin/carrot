### 资源加载

### 设定启动配置

### 1. MainMenuScene

1. 播放背景音乐
playMusic res.sd_mm_BGMusic_mp3

2. 加载背景层
Sprite res/MainMenu/zh/front_bg.png
// 居中
setPosition(cc.winSize.width / 2, cc.winSize.height / 2);

3. 加载主层
    1. 读取配置：isUpUnlock: 从localStorage中读取 ”IS_UP_UNLOCK_KEY“配置
    2. 开始冒险 (居中，有偏移(-8, 75))
        - 点击以后；播放点击声效
        - 切换到关卡选择界面
       天天向上 (居中有偏移, (-8, -45))
        - 点击 播放声效
        - 根据lock状态决定是否发送 “弹出层”的消息
       锁定时候，显示锁头的图片
       弹出层页面：
            - 新建一个layer，winSize大小，可触摸
            - 加载背景图片， 居中全局
            - 加载文字精灵， 居中 偏移(0, 100)
            - 加载确定按钮 点击以后： 存储lock状态，清除锁的精灵
            - 加载取消的按钮， 点击以后隐藏弹出层
    3. "设置"按钮的加载
        - 上下移动的动画
    4. "帮助"按钮
        - 左右摇摆
        - 身体上下移动
    5. 底部怪物
        - 左侧黄色的 上下移动
        - 左侧绿色的 左右移动
    6. 云朵的添加
    7. 前排的怪物
        - 右侧黄色 左右移动
        - 左侧蓝色 上下移动
    8. 前排的云朵
    9. 加载萝卜 一个曲线动画，渐渐变大
    10. 加载前景

// 加载菜单
Sprite("res/MainMenu/zh/front_btn_start_normal.png");
Sprite("res/MainMenu/zh/front_btn_start_pressed.png");
Sprite("res/MainMenu/zh/front_btn_start_normal.png");
// 默认，点击，禁用状态的按钮
// 回调： 播放选择音乐，打log 切换到 选择关卡的页面。
设定位置
// 天天向上按钮
var start = new cc.MenuItemSprite(
            startNormal,
            startPress,
            startDisabled,
            function(){
                cc.audioEngine.playEffect(res.sd_mm_Select_mp3);
                cc.log("点击【开始冒险】按钮");
                cc.director.runScene(new ChooseLevelScene());
            }.bind(this));
        start.setPosition(cc.winSize.width / 2 - 8, cc.winSize.height / 2 + 75);

        // 天天向上
        var floorNormal=new cc.Sprite("res/MainMenu/zh/front_btn_floor_normal.png");
        var floorPress  = new cc.Sprite("res/MainMenu/zh/front_btn_floor_pressed.png");
        var floorDisabled  = new cc.Sprite("res/MainMenu/zh/front_btn_floor_normal.png");
        var floor = new cc.MenuItemSprite(
            floorNormal,
            floorPress,
            floorDisabled,
            function(){
                cc.audioEngine.playEffect(res.sd_mm_Select_mp3);
                if (this.isUpUnlock == "NO") {
                    // 分发事件[打开解锁面板]
                    cc.eventManager.dispatchEvent(new cc.EventCustom(jf.EventName.OPEN_UNLOCK_UP_LAYER));
                } else {
                    cc.log("TODO: 实现【天天向上】按钮功能");
                }
            }.bind(this));
        floor.setPosition(cc.winSize.width / 2 - 8, cc.winSize.height / 2 - 45);


        if (this.isUpUnlock == "NO") {
            var upLock = new cc.Sprite("res/MainMenu/front_btn_floor_locked.png");
            floor.addChild(upLock);
            this.upLock = upLock;
            upLock.setPosition(floor.width + 5, floor.height / 2 + 25);
        }

        var menu = new cc.Menu(start, floor);
        this.addChild(menu);
        menu.setPosition(0, 0);